package com.armenta.examenkotlin


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {

    // TERMINADO
    // Declaración de componentes
    private lateinit var txtNumeroCuenta : EditText;
    private lateinit var txtNombre : EditText;
    private lateinit var txtBanco : EditText;
    private lateinit var txtSaldo : EditText;
    private lateinit var btnEnviar : Button;
    private lateinit var btnSalir : Button;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        instanciarComponentes()

        this.btnEnviar.setOnClickListener { this.clic_btnEnviar() }
        this.btnSalir.setOnClickListener { this.clic_btnSalir() }
    }


    private fun instanciarComponentes(){
        this.txtNumeroCuenta = findViewById(R.id.txtNumeroCuenta)
        this.txtNombre = findViewById(R.id.txtNombre)
        this.txtBanco = findViewById(R.id.txtBanco)
        this.txtSaldo = findViewById(R.id.txtSaldo)

        this.btnEnviar = findViewById(R.id.btnEnviar)
        this.btnSalir = findViewById(R.id.btnSalir)
    }


    private fun clic_btnEnviar(){

        var strNumeroCuenta: String = this.txtNumeroCuenta.text.toString();
        var strNombre: String = this.txtNombre.text.toString();
        var strBanco: String = this.txtBanco.text.toString();
        var strSaldo: String = this.txtSaldo.text.toString();

        if(!strNumeroCuenta.equals("") && !strNombre.equals("") && !strBanco.equals("") && !strSaldo.equals("")){


            var bundle = Bundle()
            bundle.putString("strNumeroCuenta", strNumeroCuenta)
            bundle.putString("strNombre", strNombre)
            bundle.putString("strBanco", strBanco)
            bundle.putString("strSaldo", strSaldo)


            var intento = Intent(this@MainActivity, cuentaBancoActivity::class.java)
            intento.putExtras(bundle)


            startActivity(intento)


            this.txtNumeroCuenta.setText("")
            this.txtNombre.setText("")
            this.txtBanco.setText("")
            this.txtSaldo.setText("")

        }
        else Toast.makeText(applicationContext, "Todas los campos son necesarios.", Toast.LENGTH_SHORT).show()

    }



    private fun clic_btnSalir(){
        var confirmar = AlertDialog.Builder(this)

        confirmar.setTitle("NACIONAL SOMEX")
        confirmar.setMessage("¿Decea cerrar la aplicación?")

        confirmar.setPositiveButton("Confirmar"){
                dialogInterface, which->finish()
        }

        confirmar.setNegativeButton("Cancelar"){
                dialogInterface, which->
        }

        confirmar.show()
    }
}//Se programó el MainActivity.
