package com.armenta.examenkotlin

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity


class CuentaBancoActivity : AppCompatActivity() {
    private var strBanco: TextView? = null
    private var lblNombre: TextView? = null
    private var lblSaldo: TextView? = null
    private var lblMovimientos: TextView? = null
    private var txtCantidad: EditText? = null
    private var btnDeposito: Button? = null
    private var btnRetiro: Button? = null
    private var btnRegresar: Button? = null
    private var cuenta: CuentaBanco? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuenta_banco)
        iniciarComponentes()

        // Obtener los datos del MainActivity
        val datos = intent.extras
        val nombre = datos!!.getString("nombre")
        val numCuenta = datos.getString("numCuenta")
        val banco = datos.getString("banco")
        val saldo = datos.getString("saldo")
        cuenta = CuentaBanco(numCuenta!!.toInt(), nombre!!, banco!!, saldo!!.toFloat())
        // Actualizar los componentes de la interfaz de usuario con los datos recibidos
        strBanco!!.text = banco
        lblNombre!!.text = nombre
        lblSaldo!!.text = saldo
    }

    private fun iniciarComponentes() {
        strBanco = findViewById<TextView>(R.id.strBanco)
        lblNombre = findViewById<TextView>(R.id.lblNombre)
        lblSaldo = findViewById<TextView>(R.id.lblSaldo)
        lblMovimientos = findViewById<TextView>(R.id.lblMovimientos)
        txtCantidad = findViewById<EditText>(R.id.txtCantidad)
        btnDeposito = findViewById<Button>(R.id.btnDeposito)
        btnRetiro = findViewById<Button>(R.id.btnRetiro)
        btnRegresar = findViewById<Button>(R.id.btnRegresar)
    }

    private fun validarCampos(): Boolean {
        val cantidad = txtCantidad!!.text.toString()
        return !cantidad.isEmpty()
    }

    fun deposito(v: View?) {
        if (validarCampos()) {
            val cantidad = txtCantidad!!.text.toString().toFloat()
            val saldoActual = lblSaldo!!.text.toString().toFloat()
            val nuevoSaldo = saldoActual + cantidad
            cuenta!!.depositar(cantidad)
            lblSaldo!!.text = java.lang.Float.toString(cuenta!!.getSaldo())
            txtCantidad!!.setText("")
            mostrarToast("Depósito realizado correctamente")
        } else {
            mostrarToast("Por favor, completa todos los campos")
        }
    }

    fun retiro(v: View?) {
        val cantidad = txtCantidad!!.text.toString().toFloat()
        val saldoActual = lblSaldo!!.text.toString().toFloat()
        if (validarCampos()) {
            if (cantidad > saldoActual) {
                mostrarToast("No hay suficiente saldo para realizar el retiro")
            } else {
                val nuevoSaldo = saldoActual - cantidad
                cuenta!!.retirar(cantidad)
                lblSaldo!!.text = java.lang.Float.toString(cuenta!!.getSaldo())
                txtCantidad!!.setText("")
                mostrarToast("Retiro realizado correctamente")
            }
        } else {
            mostrarToast("Por favor, completa todos los campos")
        }
    }

    fun regresar(v: View?) {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Banco")
        confirmar.setMessage("Regresar al MainActivity?")
        confirmar.setPositiveButton(
            "Confirmar"
        ) { dialog, which -> finish() }
        confirmar.setNegativeButton(
            "Cancelar"
        ) { dialog, which -> dialog.dismiss() }
        confirmar.show()
    }

    private fun mostrarToast(mensaje: String) {
        Toast.makeText(applicationContext, mensaje, Toast.LENGTH_SHORT).show()
    }
}
//Terminacion del proyecto y se verificó.